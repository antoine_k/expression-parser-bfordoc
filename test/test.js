const ConditionParser = require('../index.js');
const assert = require('assert');

function equal(testname, expression, result, facts = null) {
    it(testname, () => {
        return ConditionParser.compute(expression, facts).then(data => {
            assert.equal(data, result);
        });
    });
}

let uniqueFacts = new Map();
uniqueFacts.set('firstname', 'christo');
uniqueFacts.set('list_firstname', [1, 2, 3, 'christo', 'victor']);
uniqueFacts.set('salary', '3000');
uniqueFacts.set('age', 15);
uniqueFacts.set('address', {
    city: 'Bordeaux',
    zipcode: '33300',
    phone: {
        mobile: '0606060606',
        fixe: '0505050505' //no way to remember how the fuck we call it in english
    }
});

let multipleFacts = new ConditionParser.FactManager(ConditionParser.FactManager.Modes.Multiple);
multipleFacts.set('cars_price',
    [
        {
            type: 'number',
            value: 2000,
            attributes: {color: 'blue', 'brand': 'Honda'}
        },
        {
            type: 'number',
            value: 500,
            attributes: {color: 'red', 'brand': 'Honda'}
        },
        {
            type: 'number',
            value: 5000,
            attributes: {color: 'black', 'brand': 'Mercedes', speed: 1000}
        },
        {

            type: 'object',
            value: {
                new: 10000,
                old: 100
            },
            attributes: {color: 'yellow', 'brand': 'Mercedes', speed: 1000}
        },
        {
            type: 'object',
            value: {
                model: {
                    explosion: 10000
                }
            },
            attributes: {color: 'yellow', 'brand': 'Honda', speed: 1000}
        }
    ]);
multipleFacts.set('firstname', {value: 'christo', type: 'string'});
multipleFacts.set('list_firstname', {value: [1, 2, 3, 'christo', 'victor'], type: 'array'});
multipleFacts.set('salary', {value: '3000', type: 'number'});
multipleFacts.set('age', {value: 15, type: 'number'});
multipleFacts.set('address', {
    type: 'object',
    value: {
        city: 'Bordeaux',
        zipcode: '33300',
        phone: {
            mobile: '0606060606',
            fixe: '0505050505' //no way to remember how the fuck we call it in english
        }
    }
});
multipleFacts.set('mainBrand', {
    type: 'string',
    value: 'Honda'
});


describe('Operator', () => {
    equal('<', '2<4', true);
    equal('>', '5>1', true);
    equal('==', '2==2', true);
    equal('!=', '2!=4', true);
    equal('And', '2==2 and 5 < 12', true);
    equal('Or', '2==3 or 10 < 12', true);
    equal('In', '\'christo\' in [\'alfred\', \'victor\', \'christo\'] and 12 in [1, 2, 3, 12]', true);
});

describe('Arithmetic operation', () => {
    equal('Addition', '2+2', 4);
    equal('Subtraction', '2-2', 0);
    equal('Multiplication', '2*2', 4);
    equal('Division', '2/2', 1);
    equal('Modulo', '15%4', 3);
    equal('Minimum', 'min(18.7, 12.5, 12.1, 14)', 12.1);
    equal('Maximum', 'max(12, 18.5, 14)', 18.5);
    equal('Complex operation', '-1245*987-986+4547/8744-9877+6441/87544*6544%2', -1239676.008908875);
    equal('Complex operation (with parentheses)', '1245*((987-986)+4547%3/(-8744-9877)%45+6441/87544*6544)', 600676.3576443489);
});

describe('Basic Facts', () => {
    equal('Simple fact', 'firstname', 'christo', uniqueFacts);
    equal('Fact in array', 'firstname in [\'christo\', \'victor\', 12]', true, uniqueFacts);
    equal('Fact in array that contain fact', 'firstname in [firstname, \'victor\', 12]', true, uniqueFacts);
    equal('Fact in array of fact', 'firstname in list_firstname', true, uniqueFacts);
    equal('Fact with object', 'address|city', 'Bordeaux', uniqueFacts);
    equal('Fact with object nested', 'address|phone.mobile', '0606060606', uniqueFacts);
});

describe('Complex Facts', () => {
    equal('Simple fact', 'firstname', 'christo', multipleFacts);
    equal('Fact in array', 'firstname in [\'christo\', \'victor\', 12]', true, multipleFacts);
    equal('Fact in array that contain fact', 'firstname in [firstname, \'victor\', 12]', true, multipleFacts);
    equal('Fact in array of fact', 'firstname in list_firstname', true, multipleFacts);
    equal('Fact with object', 'address|city', 'Bordeaux', multipleFacts);
    equal('Fact with object nested', 'address|phone.mobile', '0606060606', multipleFacts);
    equal('Fact with filter', 'cars_price[color="red", brand="Honda"]', 500, multipleFacts);
    equal('Fact with filter and object', 'cars_price|old[speed=1000,brand="Mercedes",color="yellow"]', 100, multipleFacts);
    equal('Fact with filter and object nested', 'cars_price|model.explosion[speed=1000,brand="Honda",color="yellow"]', 10000, multipleFacts);
    equal('Fact with filter of filter and object nested', 'cars_price|model.explosion[speed=1000,brand=mainBrand,color="yellow"]', 10000, multipleFacts);
    equal('Fact in array that contain fact with filter and object', '10000 in [cars_price|model.explosion[speed=1000,brand=\'Honda\',color=\'yellow\'], \'victor\', 12]', true, multipleFacts);
    equal('Fact in array that contain fact with filter and object and max', 'max(10, cars_price[speed=1000,brand=\'Mercedes\',color=\'black\'], cars_price|model.explosion[speed=1000,brand=\'Honda\',color=\'yellow\'])', 10000, multipleFacts);
});

describe('Complex expression', () => {
    equal('Expression 1', 'firstname == "christo" and cars_price|old[speed=1000,brand="Mercedes",color="yellow"] <= 50 + 50', true, multipleFacts);
    equal('Expression 2', 'age > 18 and address|city == "bordeaux" or salary > cars_price[color="blue", brand="Honda"] + cars_price[color="red", brand="Honda"]', true, multipleFacts);
});
