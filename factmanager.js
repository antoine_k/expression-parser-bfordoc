'use strict';

const {FactError, ...ErrorType} = require('./error.js');

const FactType = {
    Array: 'array',
    String: 'string',
    Object: 'object',
    Number: 'number',
    Boolean: 'boolean',
    Fact: 'fact',
    Null: null
};

const Modes = {
    'Unique': 'unique',
    'Multiple': 'multiple'
};

class FactManager {
    constructor(mode = Modes.Unique) {
        this.mode = mode;
        this.facts = new Map();
    }

    init(facts) {
        this.clear();
        this.facts = new Map(facts);
    }

    clear() {
        this.facts.clear();
    }

    set(key, value) {
        if (this.mode === Modes.Unique) {
            this.facts.set(key, value);
        } else if (Array.isArray(value)) {
            for (let item of value) {
                this.set(key, item);
            }
        } else {
            if (!value.hasOwnProperty('type') || !value.hasOwnProperty('value')) {
                throw new FactError(ErrorType.ExpressionMalformatted, 'Fact badly formatted. When using multiple mode, fact should contain a value and type properties', value);
            }
            const facts = this.get(key);
            if (facts !== undefined) {
                if (Array.isArray(facts)) {
                    facts.push(value);
                } else {
                    this.facts.set(key, [facts, value]);
                }
            } else {
                this.facts.set(key, value);
            }
        }
    }

    has(factSought) {
        return this.get(factSought) !== undefined;
    }

    get(factSought) {
        return this.facts.get(factSought);
    }

    getValue(factSought) {
        if (!this.facts) {
            throw new FactError(ErrorType.FactNotFound, 'No facts were passed to the context');
        }
        let factFound = this.facts.get(factSought.value);
        if (factFound === undefined || factFound === null) {
            throw new FactError(ErrorType.FactNotFound, 'Fact not found', factSought);
        }
        if (this.mode === Modes.Multiple) {
            return this._matchMultiple(factFound, factSought);
        } else {
            return this._match(factFound, factSought);
        }
    }

    // -- Internal methods

    _getValue(fact) {
        if (this.mode === Modes.Multiple) {
            if (fact.type === FactType.String) {
                return String(fact.value);
            } else if (fact.type === FactType.Number) {
                return Number(fact.value);
            } else if (fact.type === FactType.Boolean) {
                return Boolean(fact.value);
            } else {
                return fact.value;
            }
        } else {
            return fact;
        }
    }

    _matchObject(factFound, factSought) {
        if (factSought.nested) {
            let traverse = this._getValue(factFound);
            for (let prop of factSought.nested) {
                if (traverse[prop] === undefined) {
                    throw new FactError(ErrorType.NestedObjectPathMismatch, 'Nested path does not match the expression', factSought, factFound);
                }
                traverse = traverse[prop];
            }
            return traverse;
        } else {
            return this._getValue(factFound);
        }
    }

    _match(factFound, factSought) {
        if (factSought.nested) {
            return this._matchObject(factFound, factSought);
        } else {
            return this._getValue(factFound);
        }
    }

    _matchMultiple(factFound, factSought) {
        if (Array.isArray(factFound)) {
            for (let fact of factFound) {
                if (this._matchAttribute(fact, factSought)) {
                    return this._match(fact, factSought);
                }
            }
        } else {
            if (this._matchAttribute(factFound, factSought)) {
                return this._match(factFound, factSought);
            }
        }
        throw new FactError(ErrorType.AttributesMismatch, 'Fact found but attributes does not match the fact sought', factFound, factSought);
    }


    _matchAttribute(fact, factSought) {
        if (fact.attributes === undefined || fact.attributes === null || Object.keys(fact.attributes).length === 0) {
            return factSought.attributes === null || factSought.attributes === undefined || Object.keys(factSought).length === 0;
        } else if (factSought.attributes === undefined || factSought.attributes === null || Object.keys(factSought.attributes).length === 0) {
            return fact.attributes === null || fact.attributes === undefined || Object.keys(fact.attributes).length === 0;
        } else {
            return this._isMatchingAttributes(fact.attributes, factSought.attributes);
        }
    }

    _isMatchingAttributes(factAttributes, factSoughtAttributes) {
        const keysfactSoughtAttributes = Object.keys(factSoughtAttributes);

        if (Object.keys(factAttributes).length !== keysfactSoughtAttributes.length) {
            return false;
        }
        for (let [key, value] of Object.entries(factAttributes)) {
            if (!keysfactSoughtAttributes.includes(key)) {
                return false;
            }
            if (factSoughtAttributes[key].type === FactType.Fact) {
                try {
                    if (this.getValue(factSoughtAttributes[key]) !== value) {
                        return false;
                    }
                } catch (e) {
                    return false;
                }
            } else if (factSoughtAttributes[key].value !== value) {
                return false;
            }
        }

        return true;
    }

    static get Modes() {
        return Modes;
    }
}

module.exports = FactManager;
