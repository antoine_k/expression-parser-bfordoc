"use strict";

const errors = {
    FactNotFound: 'FactNotFound',
    ExpressionMalformatted: 'ExpressionMalformatted',
    AttributesMismatch: 'AttributesMismatch',
    NestedObjectPathMismatch: 'NestedObjectPathMismatch',
    MultipleFactMatched: 'MultipleFactMatched'
};

class ExpressionError extends Error {
    constructor(message) {
        super(message);
        this.name = this.constructor.name;
        Error.captureStackTrace(this, this.constructor);
        this.type = errors.ExpressionMalformatted;
    }
}

class FactError extends Error {
    constructor(type, message, wantedFact, foundFact) {
        super(message);
        this.name = this.constructor.name;
        Error.captureStackTrace(this, this.constructor);
        this.type = type;
        this.data = {
            wantedFact: wantedFact,
            foundFact: foundFact
        }
    }
}

module.exports = {
    FactError,
    ExpressionError,
    ...errors
};