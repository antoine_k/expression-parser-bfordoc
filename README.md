# Condition Parser

NodeJS library to compute a logical expression

## Getting Started

To use this module in your project, install the npm package

```
npm install git+ssh://git@bitbucket.org:snarkfactory/condition-parser.git#master
```

### Usage

```
const Condition = require('condition-parser');

let facts = new Map();
facts.set('age', 26);
facts.set('sexe', "homme");
facts.set('weight', 51);

let condition = '(age == 25 and sexe == "homme") or weight >= 50';

Condition.compute(condition, facts)
    .then(v => console.log(v))
    .catch(e => console.log(e))

Condition.parse(condition)
    .then(tree => {
        console.log(Condition.getFacts(tree));
        Condition.computeTree(tree, facts)
            .then(v => console.log(v))
            .catch(e => console.log(e))
    }).catch(e => console.log(e));
```

The internal FactManager use the "Unisque" mode by default but you can use it in complex mode and give multiple fact for one key.

```
const Condition = require('condition-parser');

const facts = new Condition.FactManager(Condition.FactManager.Modes.Multiple);
facts.set('address', {
                         type: 'object',
                         value: {
                             city: 'Bordeaux',
                             zipcode: '33300',
                             phone: {
                                 mobile: '0606060606',
                                 fixe: '0505050505'
                             }
                     });
facts.set('cars_price', [
                          {
                              type: 'number',
                              value: 2000,
                              attributes: {color: 'blue', 'brand': 'Honda'}
                          },
                          {
                              type: 'number',
                              value: 500,
                              attributes: {color: 'red', 'brand': 'Honda'}
                          },
                          {
                              type: 'number',
                              value: 5000,
                              attributes: {color: 'black', 'brand': 'Mercedes', speed: 1000}
                          },
                          {

                              type: 'object',
                              value: {
                                  new: 10000,
                                  old: 100
                              },
                              attributes: {color: 'yellow', 'brand': 'Mercedes', speed: 1000}
                          },
                          {
                              type: 'object',
                              value: {
                                  model: {
                                      explosion: 10000
                                  }
                              },
                              attributes: {color: 'yellow', 'brand': 'Honda', speed: 1000}
                          }
        ]);
facts.set('weight', 51);

const condition = "10000 in [cars_price|model.explosion[speed=1000,brand=Honda,color=yellow], 'victor', 12]";

Condition.compute(condition, facts)
    .then(v => console.log(v))
    .catch(e => console.log(e))

Condition.parse(condition)
    .then(tree => {
        console.log(Condition.getFacts(tree));
        Condition.computeTree(tree, facts)
            .then(v => console.log(v))
            .catch(e => console.log(e))
    }).catch(e => console.log(e));
```

### Compiling parser.js

To generate the parser you need to use the cli tool of Jison

```
npm install jison -g
jison  grammar.y -o parser.js
```
