"use strict";

const Parser = require('./parser.js').parser;
const operation = require('./operation.js');
const error = require('./error.js');
const ExpressionError = error.ExpressionError;
const FactError = error.FactError;
const FactManager = require('./factmanager.js');

function parse(condition) {
    return new Promise((resolve, reject) => {
        try {
            return resolve(Parser.parse(condition));
        } catch (e) {
            return reject(new ExpressionError(e));
        }
    })
}

function extractFacts(node) {
    let facts = [];
    if (node.type === 'fact') {
        facts.push({
            code: node.nested && node.nested.length ? node.value + '|' + node.nested.join('.') : node.value,
            base: node.value,
            attributes: node.attributes ? node.attributes : null,
            nested: node.nested ? node.nested : null,
            source: node.source ? node.source : null
        })
    } else if (node.hasOwnProperty('args')) {
        for (let arg of node.args) {
            facts = facts.concat(extractFacts(arg))
        }
    } else {
        return facts
    }
    return facts;
}

function compute(condition, facts = new Map(), mode = FactManager.Modes.Unique) {
    return this.parse(condition)
        .then(tree => computeTree(tree, facts, mode))
}

function computeTree(node, facts = new Map(), mode = FactManager.Modes.Unique) {
    return new Promise((resolve, reject) => {
        try {
            if (!(facts instanceof FactManager)) {
                let factManager = new FactManager(mode);
                factManager.init(facts);
                return resolve(resolveTree(node, factManager))
            }
            return resolve(resolveTree(node, facts))
        } catch (e) {
            return reject(e)
        }
    })
}

function resolveTree(node, facts) {
    let action = operation.getOperation(node);
    if (node.hasOwnProperty('args')) {
        let args = [];
        for (let elem of node.args) {
            args.push(resolveTree(elem, facts))
        }
        return action(args, facts);
    } else {
        return action(node, facts);
    }
}

module.exports = {parse, extractFacts, compute, computeTree, FactManager, ExpressionError, FactError};
