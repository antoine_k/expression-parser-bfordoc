/*
 *
 *  grammar to generate the parser, use jison cli to generate the parser.js
 *
 */

/* lexical grammar */
%lex
%%

\s+                     /* skip whitespace */
"*"                     return '*'
"/"                     return '/'
"-"                     return '-'
"+"                     return '+'
"%"                     return '%'
"("                     return '('
")"                     return ')'
"=="                    return '=='
"!="                    return '!='
">="                    return '>='
"<="                    return '<='
">"                     return '>'
"<"                     return '<'
"and"                   return 'and'
"or"                    return 'or'
"in"                    return 'in'
"min"                   return 'min'
"max"                   return 'max'
"["                     return '['
"]"                     return ']'
"{"                     return '{'
"}"                     return '}'
","                     return ','
"="                     return '='
"|"                     return '|'
"."                     return '.'
"true"                  return 'true'
"false"                 return 'false'
\"[^\"]*\"|\'[^\']*\'   return 'STRING'
\b[0-9]+(?!\.[\w]+)\b   return 'INTEGER'
\b[0-9]+\.[0-9]+\b      return 'FLOAT'
\b[\w]+\b               return 'TOKEN'
<<EOF>>                 return 'EOF'

/lex

/* operator associations and precedence */

%left 'or'
%left 'and'
%left 'in'
%left '==' '!='
%left '>' '>='
%left '<' '<='
%left '+' '-'
%left '*' '/' '%'
%right 'UMINUS'

%start expressions

%% /* language grammar */

expressions:
    e EOF
        { return $1; }
    ;

e:
    e '+' e
        {$$ = {
                type: 'addition',
                args: [$1, $3]
              }
        }
    | e '-' e
        {$$ = {
                type: 'subtraction',
                args: [$1, $3]
              }
        }
    | e '*' e
        {$$ = {
                type: 'multiplication',
                args: [$1, $3]
              }
        }
    | e '/' e
        {$$ = {
                type: 'division',
                args: [$1, $3]
              }
        }
    | e '%' e
        {$$ = {
                type: 'modulo',
                args: [$1, $3]
              }
        }
    | e '==' e
        {$$ = {
                type: 'equality',
                args: [$1, $3]
              }
        }
    | e '!=' e
        {$$ = {
                type: 'inequality',
                args: [$1, $3]
              }
        }
    | e '<' e
        {$$ = {
                type: 'less',
                args: [$1, $3]
              }
        }
    | e '>' e
        {$$ = {
                type: 'greater',
                args: [$1, $3]
              }
        }
    | e '<=' e
        {$$ = {
                type: 'less-equal',
                args: [$1, $3]
              }
        }
    | e '>=' e
        {$$ = {
                type: 'greater-equal',
                args: [$1, $3]
              }
        }
    | 'min' '(' list_elem ')'
        {$$ = {
                type: 'minimum',
                args: $3
              }
        }
    | 'max' '(' list_elem ')'
        {$$ = {
                type: 'maximum',
                args: $3
              }
        }
    | '(' e ')'
        {$$ = $2;}
    | e 'and' e
        {$$ = {
                type: 'and',
                args: [$1, $3]
              }
        }
    | e 'or' e
        {$$ = {
                type: 'or',
                args: [$1, $3]
              }
        }
    | e 'in' list
        {$$ = {
                type: 'in',
                args: [$1, $3]
              }
        }
    | elem
        {$$ = $1;}
    ;

list:
    fact
    | '[' list_elem ']'
        {$$ = {
            'type': 'array',
            'args': $list_elem
        }}
    ;

list_elem:
    elem ',' list_elem
        {$$ = [].concat($1, $3);}
    | elem
        {$$ = [$1];}
    ;

elem:
    STRING
        {$$ = {
            type: 'string',
            value: String($1.slice(1, -1))
            }
        }
    | number
        {$$ = {
            type: 'number',
            value: $1
            }
        }
    | boolean
        {$$ = {
            type: 'boolean',
            value: $1
            }
        }
    | fact
    ;

boolean:
    'true'
        {$$ = true;}
    | 'false'
        {$$ = false;}
    ;

number:
    '-' numeral %prec UMINUS
            {$$ = -$2}
    | numeral
    ;

numeral:
    INTEGER
        {$$ = Number($1)}
    | FLOAT
        {$$ = Number($1)}
    ;

fact:
    TOKEN '[' filters ']'
        {$$ = {
                type: 'fact',
                value: $1,
                attributes: $filters
            }
        }
    | TOKEN '|' nested '[' filters ']'
        {$$ = {
                type: 'fact',
                value: $1,
                nested: $nested,
                attributes: $filters
            }
        }
    | TOKEN '|' nested
        {$$ = {
                type: 'fact',
                value: $1,
                nested: $nested
            }
        }
    | TOKEN
        {$$ = {
                type: 'fact',
                value: $1
            }
        }
    ;

nested:
    TOKEN '.' nested
        {$$ = [].concat($1, $3);}
    | TOKEN
        {$$ = [$1]}
    ;

filters:
    filter
    | filters ',' filter
        {$$ = Object.assign($1, $3)}
    ;

filter:
    TOKEN[key] '=' elem[value]
        {$$ = { [$key]: $value }}
    ;
