"use strict";

const addition = function ([a, b]) {
    return Number(a) + Number(b)
};

const subtraction = function ([a, b]) {
    return Number(a) - Number(b)
};

const division = function ([a, b]) {
    return Number(a) / Number(b)
};

const multiplication = function ([a, b]) {
    return Number(a) * Number(b)
};

const modulo = function ([a, b]) {
    return Number(a) % Number(b)
};

const equality = function ([a, b]) {
    return a == b
};

const inequality = function ([a, b]) {
    return a != b
};

const less = function ([a, b]) {
    return a < b
};

const greater = function ([a, b]) {
    return a > b
};

const lessOrEqual = function ([a, b]) {
    return a <= b
};

const greaterOrEqual = function ([a, b]) {
    return a >= b
};

const andCondition = function ([a, b]) {
    return a && b
};

const orCondition = function ([a, b]) {
    return a || b
};

const inArray = function ([a, b]) {
    return b.indexOf(a) !== -1
};

const minimum = function(a) {
    a.sort((x, y) => {
        if (x < y) {
            return -1;
        } else if (x > y) {
            return 1;
        }
        return 0;
    });
    return a[0];
};

const maximum = function(a) {
    a.sort((x, y) => {
        if (x < y) {
            return 1;
        } else if (x > y) {
            return -1;
        }
        return 0;
    });
    return a[0];
};

const getValue = function (a) {
    switch (a.type) {
        case 'string':
            return String(a.value);
        case 'number':
            return Number(a.value);
        case 'boolean':
            return Boolean(a.value);
        default:
            return a.value;
    }
};

const getFact = function (factSought, factManager) {
    return factManager.getValue(factSought);
};

const getType = function (a) {
    return a.type;
};

const getArray = function (a) {
    return a;
};

const operationType = {
    'addition': addition,
    'subtraction': subtraction,
    'division': division,
    'multiplication': multiplication,
    'modulo': modulo,
    'equality': equality,
    'inequality': inequality,
    'less': less,
    'greater': greater,
    'less-equal': lessOrEqual,
    'greater-equal': greaterOrEqual,
    'and': andCondition,
    'or': orCondition,
    'in': inArray,
    'string': getValue,
    'number': getValue,
    'boolean': getValue,
    'fact': getFact,
    'array': getArray,
    'minimum': minimum,
    'maximum': maximum,
};

module.exports = {
    getOperation: (action) => {
        return operationType[getType(action)]
    }
};